% A Year In The Life Of GTK
% Emmanuele Bassi ­:: linux.conf.au 2022

::: notes
Abstract: In December 2020 the GTK project released the new major version of the toolkit after more than four years of work, and 9 years after the previous major API version. This was a major milestone in the history of the project, but it doesn't mean work has stopped. In the past year, GTK developers made two additional minor releases, including: a whole new and improved GL-based renderer; a whole set of additional CSS properties to affect how UI elements can draw their content; new visual cues for entering text on non-English keyboard layouts; and a completely revamped documentation stack that tries to bridge the distance between the underlying C API and the various languages that can be used to write GTK applications. In this presentation we're going to catch up with the current state of GTK 4, and where we're going in the future.
:::

## Who am I?

::: notes
I am part of the GTK development team, and have been working on GTK and GNOME for about 15 years, now. I am mostly involved in the core application development platform, but I also work on the developers documentation; maintain applications; and collaborate with other GNOME maintainers in the release team. My contributions have been both part of my literal job description and done on my spare time, so I am able to appreciate the use of GTK and GNOME as a downstream developer and as a volunteer.
:::

----

## Why am I here?

:::notes
GTK is an old project. It's been in continuous development since 1996, when it was written as part of the GNU image manipulation program to replace Motif. Over the past 25 years it has seen three major version bumps—from 1 to 2, to 3, to 4—and has been used and deployed to millions of devices all around the world. The last major release, GTK 4, is probably the biggest leap in terms of the toolkit's design since the 2.0 days—a change driven both by the evolution of windowing systems on Linux, and by the challenges and realities of maintaining a volunteer-driven free software toolkit with no central authority and minimal corporate backing.
:::

----

## What am I talking about?

:::notes
So, today I'm going to talk a little bit about the changes that led to the GTK 4.0 release in 2020, and what happened to the project, and around it, in the year since then.
:::

----

# Part I: GTK4

----

## Where were we in 2016

::: notes
The main topic of 2016 was whether we could improve the rendering pipeline for GTK3 without breaking its API; but, the fact of the matter was that we had been changing the internals of the toolkit a lot more than what we did back in 2006, when we introduced Cairo in its rendering pipeline and we moved away from the internal drawing API that mimicked Xlib. We were also looking at a transition period in which we'd say goodbye to X11 and hello to Wayland, and on the hardware side of things we were seeing the adoption of high DPI density displays on laptops and desktops, as well as dedicated graphical hardware with 3D acceleration. During the GTK3 cycle we did change a lot of how the rendering pipeline worked, to allow proper transparency between widgets, as well as implementing things like CSS borders and shadows that extended outside the widget's area. This required little to no changes in idiomatic code, but there was a lot of non-idiomatic code in applications ported from GTK2 to early GTK3. We did not make ourselves any friends with application developers that consumed the platform 3 years at a time, following long-term support releases.
:::

----

## Cairo

::: notes
The main problem was, effectively, Cairo. While it introduced high quality rendering to GUIs, it was also limited by design. Like Firefox discovered, it is effectively easier to take whatever state Cairo has, blow it away, and replace it entirely with the CSS state, if you're rendering something that looks a lot like a GUI, as opposed to a vector image. Another problem at the core of Cairo was its reliance on a fast read-back pipeline between the CPU and system memory—something that just doesn't exist in modern systems using a GPU. Finally, the maintenance status of the project had been in question ever since Intel poached the two people responsible for maintaining the library. Cairo was an API that made sense as an internal detail of a toolkit, but it was exposed to library and application developers, and that made it necessary to write a wrapper around it, to represent a full set of rendering operations. At that point, though, why would you be using Cairo anyway, when it did not give you any access to the most efficient hardware in your system?
:::

----

## OpenGL

::: notes
Sadly, the only actual API that let you access that hardware is OpenGL, which has been known to be a disaster zone for years, especially on Linux. Luckily, the introduction of mainstream X11 compositors using OpenGL had the effect of improving the GL driver support, which in turn pushed more people to make use of GL for applications and games; this was coupled with improvements in the GL standard API, which jettisoned some of the old cruft from the '90s and pushed for handling the GPU like a programmable resource, instead of an opaque state machine. A CSS state renderer is much more tractable than a fully generic vector API, and you can get a lot of mileage out of a bunch of small shaders; this was demonstrated by projects like Servo's webrender, which acted as an inspiration for the GTK rendering pipeline. For instance, once you have a full tree of rendering operations, you can decide what changed from the previous frame, what didn't, and what to remove since won't ever be visible; at that point, re-rendering everything is actually faster than getting a bunch of caches to expire and then blend a bunch of existing surfaces on the CPU before sending it to the display server, which will put it on the GPU anyway. Suddenly, the rendering phase is not the bottleneck in a frame any more, which gives more time to do layout, and time to business logic in your application, while still achieving 60 frames per second.
:::

----

## HiDPI

::: notes
Pushing 60 frames per second becomes harder, and dependent on dedicated hardware, when you start considering really high resolutions; your CPU could probably do that if you're pushing 800×600 or even 1024×768, but once you get to 1080p, 2K, 4K, or even 8K, you really need something more powerful. HiDPI displays also require changing the nature of pixels from something physical to a logical unit, so you don't accidentally turn your desktop into the most challenging level of Minesweeper. This requires changing how you load image assets; how you size windowing system surface; and how you align every UI element to the pixel grid, to avoid making a blurry mess.
:::

----

## Wayland

::: notes
Alongside changes in the lower layers of the stack, the clock begun ticking for the display server technology available on Linux. Over the years toolkit have moved away from submitting rendering commands to a server, opting to send complete frames generated client-side; the X server had stood in the way of making applications reliably present windows to the user for years after the introduction of extensions like compositing. Wayland promised to make every frame a painting, with no interference by the display server.
:::

----

## Portability

::: notes
GTK acquired the ability to have additional windowing system backends for the 2.0 release. That ability was never meant to be used as a way to do cross-platform, "write once, run anywhere" development: it was a way to port GTK applications from Linux and other Unix-like systems to Windows (and macOS). This amounted to re-implementing X11 concepts on very different platforms, and it made everything more complicated to maintain, fix, and test. A simpler windowing system protocol, like Wayland, would fit in a lot better within these constraints.
:::

----

## Versioning and release policy

* Feature freeze once stable
* Small additional widgets
* Frozen CSS selectors
* Shorter API cycles
* Two supported API versions

::: notes
As I said earlier, the internal changes that happened during the GTK3 cycle didn't come for free. Of course, most people only remember the theming issues, which were never under any stability guarantee, and were mostly the result of downstream distribution of themes as packages without any additional QA process that would tie them to the release of the toolkit they were ostensibly targeting. The main impact for application developers were the changes in the drawing API, especially for applications ported in the early days of GTK3, and the extra styling for custom widgets. To avoid that happening again, just before the start of the GTK4 development process, we decided to lay down some fundamental guarantees for application developers—first and foremost that once the 4.0 release was done, the toolkit would be feature frozen; new API and new features could be added, but the existing functionality would never change—including the CSS selectors. Additionally, the new development cycle would contain a roadmap for future major API work, as well as the support window for existing stable releases, to shorten the development cycle while providing a reliable schedule.
:::

----

## Versioning and release policy

![](./gtk-versioning-scheme-1.png)

----

## What's new

* New rendering infrastructure
* OpenGL by default
* Wayland by default
* Simpler backends
* Stable feature set
* Delegation, not inheritance

----

# Part II: Documentation

----

## History

::: notes
GTK started out with hand-written TexInfo pages, before migrating to a custom documentation generator that would take comments and turn them into DocBook XML first, and HTML last. This tool, called gtk-doc, served the project well enough to be reused by a whole lot of libraries that shared the same dependencies as GTK. Sadly, gtk-doc was written in Perl, and its maintenance cost followed the curve as the language's own fall from grace. A last ditch attempt to port it to Python, as well as DocBook's performance bottleneck that made generating the API reference take as much as compiling the whole toolkit, tests and demos included, sealed gtk-doc's fate.
:::

----

## Introspection

:::notes
In the meantime, every GNOME library started providing a machine readable description of its C ABI, as well as its documentation, in the form of introspection data generated at build time. The typical consumers are language bindings, which can use it both at run time, for dynamic languages like Python or JavaScript; as well as a base for code generation, for languages like Rust or C#. Since introspection data also contains documentation, it's possible to generate a library's API reference much more efficiently. The GTK reference is now generated in a fraction of the time, with a better output, using a tool called gi-docgen.
:::

----

## Developer Documentation

![](./docs-gtk-org.png)

:::notes
The new documentation website contains not only the GTK reference, but also the reference for the GTK dependencies; it has stable, reliable, and easy to use links; it allows stable cross-linking of symbols between libraries; and, more importantly, it is easy to fix. Every symbol has a direct link to the file that contains it, and it's easy to update the documentation even through GitLab's web editor; once a change is submitted, the CI pipeline will build the documentation and publish it. The new documentation website also has client-side search using fuzzy matching on a full index of symbols and terms from the documentation data.
:::

----

## Platform documentation

![](./developer-gnome-org.png)

:::notes
On top of the revamped GTK documentation, the GNOME developer documentation website has been rebuilt from scratch; we moved from a custom Python 2 and Django app that would extract generated HTML from release tarballs to a Sphinx-based web site that uses plain reStructuredText. The content has been refreshed: guides and tutorials from the GNOME 2 era have been retired; examples have been updated; and there's a new style guide for writing developer documentation in a consistent manner. The website is validated and generated via a CI pipeline, and published using GitLab pages, which should lower the barrier to contribution.
:::

----

## Tutorials

![](./developer-tutorials.png)

:::notes
We are also in the process or writing a complete set of beginners tutorials that will bridge the gap between the Interface Guidelines and the API references, with plenty of examples. Additionally, we have advanced tutorials on how to properly integrate applications within GNOME using the various freedesktop.org specifications, as well as GNOME-specific extensions.
:::

----

## GNOME interface guidelines

![](./developer-hig.png)

:::notes
The interface guidelines have also been moved to a Sphinx-based solution, which makes them more accessible. They now reference the suggested widgets and UI components, so it's easier to implement the patterns that make GNOME applications integrate best with the platform.
:::

----

# Part III: 525,600 minutes

----

## Stabilisation

:::notes
The main goal of GTK 4 now becomes bug fixing, and handling the feedback from the application developers porting their projects.
:::

----

## GTK as a portable toolkit

:::notes
Additionally, we want to ensure that GTK 4 can be used by various environments and platforms, so that it's possible to easily port applications. We have aggressively simplified and improved our build system so it's possible to go from cloning just the GTK repository to a full build, including the library dependencies, on both macOS and Windows, using native toolchains. Support for those platforms is part of our CI pipeline, so we can catch eventual regressions; there are still some issues, especially on macOS, but we are much more confident about the state of GTK on Windows and macOS than we have ever been.
:::

----

## Platform libraries

:::notes
Since GTK is used by multiple environments as a foundational toolkit, we wanted to make it more platform-agnostic. Environments like Elementary and Xfce provide their own platform libraries for ensuring applications feel integrated with their guidelines. By making GTK less beholden to the GNOME design patterns, those libraries can now avoid having to undo some of the toolkit's internal state, or write entirely custom widgets to replace GTK's own.
:::

----

## Libadwaita

![](./libadwaita.png)

:::notes
GNOME still needs a way to implement the UI design patterns for its own interface guidelines; additionally, GNOME's development cycles may not match the GTK cycles—both in terms of landing new features as well as maintaining the strict API and ABI guarantees of GTK. For these reasons, the GNOME project introduced its own platform library, called "libadwaita". Libadwaita provides UI elements that implement the GNOME style and interface guidelines, and make it easier to support things like platform-wide high contrast and dark styles. Version 1.0 of libadwaita has been released alongside GTK 4.6, and it is going to be widely available with GNOME 42.
:::

----

# Thank you

----

# Questions?

----

# Resources

- GTK documentation: <https://docs.gtk.org/>
- GNOME developers documentation: <https://developer.gnome.org/>
- Libadwaita: <https://gnome.pages.gitlab.gnome.org/libadwaita/>
- gobject-introspection: <https://gi.readthedocs.org/>
- gi-docgen: <https://gnome.pages.gitlab.gnome.org/gi-docgen/>