<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <meta name="author" content="Emmanuele Bassi ­:: linux.conf.au 2022">
  <title>A Year In The Life Of GTK</title>
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
  <link rel="stylesheet" href="reveal.js/dist/reset.css">
  <link rel="stylesheet" href="reveal.js/dist/reveal.css">
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="reveal.js/dist/theme/black.css" id="theme">
</head>
<body>
  <div class="reveal">
    <div class="slides">

<section id="title-slide">
  <h1 class="title">A Year In The Life Of GTK</h1>
  <p class="author">Emmanuele Bassi ­:: linux.conf.au 2022</p>
</section>

<section class="slide level1">

<aside class="notes">
<p>Abstract: In December 2020 the GTK project released the new major version of the toolkit after more than four years of work, and 9 years after the previous major API version. This was a major milestone in the history of the project, but it doesn’t mean work has stopped. In the past year, GTK developers made two additional minor releases, including: a whole new and improved GL-based renderer; a whole set of additional CSS properties to affect how UI elements can draw their content; new visual cues for entering text on non-English keyboard layouts; and a completely revamped documentation stack that tries to bridge the distance between the underlying C API and the various languages that can be used to write GTK applications. In this presentation we’re going to catch up with the current state of GTK 4, and where we’re going in the future.</p>
</aside>
<h2 id="who-am-i">Who am I?</h2>
<aside class="notes">
<p>I am part of the GTK development team, and have been working on GTK and GNOME for about 15 years, now. I am mostly involved in the core application development platform, but I also work on the developers documentation; maintain applications; and collaborate with other GNOME maintainers in the release team. My contributions have been both part of my literal job description and done on my spare time, so I am able to appreciate the use of GTK and GNOME as a downstream developer and as a volunteer.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="why-am-i-here">Why am I here?</h2>
<aside class="notes">
<p>GTK is an old project. It’s been in continuous development since 1996, when it was written as part of the GNU image manipulation program to replace Motif. Over the past 25 years it has seen three major version bumps—from 1 to 2, to 3, to 4—and has been used and deployed to millions of devices all around the world. The last major release, GTK 4, is probably the biggest leap in terms of the toolkit’s design since the 2.0 days—a change driven both by the evolution of windowing systems on Linux, and by the challenges and realities of maintaining a volunteer-driven free software toolkit with no central authority and minimal corporate backing.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="what-am-i-talking-about">What am I talking about?</h2>
<aside class="notes">
<p>So, today I’m going to talk a little bit about the changes that led to the GTK 4.0 release in 2020, and what happened to the project, and around it, in the year since then.</p>
</aside>
</section>
<section id="part-i-gtk4" class="slide level1">
<h1>Part I: GTK4</h1>
</section>
<section class="slide level1">

<h2 id="where-were-we-in-2016">Where were we in 2016</h2>
<aside class="notes">
<p>The main topic of 2016 was whether we could improve the rendering pipeline for GTK3 without breaking its API; but, the fact of the matter was that we had been changing the internals of the toolkit a lot more than what we did back in 2006, when we introduced Cairo in its rendering pipeline and we moved away from the internal drawing API that mimicked Xlib. We were also looking at a transition period in which we’d say goodbye to X11 and hello to Wayland, and on the hardware side of things we were seeing the adoption of high DPI density displays on laptops and desktops, as well as dedicated graphical hardware with 3D acceleration. During the GTK3 cycle we did change a lot of how the rendering pipeline worked, to allow proper transparency between widgets, as well as implementing things like CSS borders and shadows that extended outside the widget’s area. This required little to no changes in idiomatic code, but there was a lot of non-idiomatic code in applications ported from GTK2 to early GTK3. We did not make ourselves any friends with application developers that consumed the platform 3 years at a time, following long-term support releases.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="cairo">Cairo</h2>
<aside class="notes">
<p>The main problem was, effectively, Cairo. While it introduced high quality rendering to GUIs, it was also limited by design. Like Firefox discovered, it is effectively easier to take whatever state Cairo has, blow it away, and replace it entirely with the CSS state, if you’re rendering something that looks a lot like a GUI, as opposed to a vector image. Another problem at the core of Cairo was its reliance on a fast read-back pipeline between the CPU and system memory—something that just doesn’t exist in modern systems using a GPU. Finally, the maintenance status of the project had been in question ever since Intel poached the two people responsible for maintaining the library. Cairo was an API that made sense as an internal detail of a toolkit, but it was exposed to library and application developers, and that made it necessary to write a wrapper around it, to represent a full set of rendering operations. At that point, though, why would you be using Cairo anyway, when it did not give you any access to the most efficient hardware in your system?</p>
</aside>
</section>
<section class="slide level1">

<h2 id="opengl">OpenGL</h2>
<aside class="notes">
<p>Sadly, the only actual API that let you access that hardware is OpenGL, which has been known to be a disaster zone for years, especially on Linux. Luckily, the introduction of mainstream X11 compositors using OpenGL had the effect of improving the GL driver support, which in turn pushed more people to make use of GL for applications and games; this was coupled with improvements in the GL standard API, which jettisoned some of the old cruft from the ’90s and pushed for handling the GPU like a programmable resource, instead of an opaque state machine. A CSS state renderer is much more tractable than a fully generic vector API, and you can get a lot of mileage out of a bunch of small shaders; this was demonstrated by projects like Servo’s webrender, which acted as an inspiration for the GTK rendering pipeline. For instance, once you have a full tree of rendering operations, you can decide what changed from the previous frame, what didn’t, and what to remove since won’t ever be visible; at that point, re-rendering everything is actually faster than getting a bunch of caches to expire and then blend a bunch of existing surfaces on the CPU before sending it to the display server, which will put it on the GPU anyway. Suddenly, the rendering phase is not the bottleneck in a frame any more, which gives more time to do layout, and time to business logic in your application, while still achieving 60 frames per second.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="hidpi">HiDPI</h2>
<aside class="notes">
<p>Pushing 60 frames per second becomes harder, and dependent on dedicated hardware, when you start considering really high resolutions; your CPU could probably do that if you’re pushing 800×600 or even 1024×768, but once you get to 1080p, 2K, 4K, or even 8K, you really need something more powerful. HiDPI displays also require changing the nature of pixels from something physical to a logical unit, so you don’t accidentally turn your desktop into the most challenging level of Minesweeper. This requires changing how you load image assets; how you size windowing system surface; and how you align every UI element to the pixel grid, to avoid making a blurry mess.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="wayland">Wayland</h2>
<aside class="notes">
<p>Alongside changes in the lower layers of the stack, the clock begun ticking for the display server technology available on Linux. Over the years toolkit have moved away from submitting rendering commands to a server, opting to send complete frames generated client-side; the X server had stood in the way of making applications reliably present windows to the user for years after the introduction of extensions like compositing. Wayland promised to make every frame a painting, with no interference by the display server.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="portability">Portability</h2>
<aside class="notes">
<p>GTK acquired the ability to have additional windowing system backends for the 2.0 release. That ability was never meant to be used as a way to do cross-platform, “write once, run anywhere” development: it was a way to port GTK applications from Linux and other Unix-like systems to Windows (and macOS). This amounted to re-implementing X11 concepts on very different platforms, and it made everything more complicated to maintain, fix, and test. A simpler windowing system protocol, like Wayland, would fit in a lot better within these constraints.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="versioning-and-release-policy">Versioning and release policy</h2>
<ul>
<li class="fragment">Feature freeze once stable</li>
<li class="fragment">Small additional widgets</li>
<li class="fragment">Frozen CSS selectors</li>
<li class="fragment">Shorter API cycles</li>
<li class="fragment">Two supported API versions</li>
</ul>
<aside class="notes">
<p>As I said earlier, the internal changes that happened during the GTK3 cycle didn’t come for free. Of course, most people only remember the theming issues, which were never under any stability guarantee, and were mostly the result of downstream distribution of themes as packages without any additional QA process that would tie them to the release of the toolkit they were ostensibly targeting. The main impact for application developers were the changes in the drawing API, especially for applications ported in the early days of GTK3, and the extra styling for custom widgets. To avoid that happening again, just before the start of the GTK4 development process, we decided to lay down some fundamental guarantees for application developers—first and foremost that once the 4.0 release was done, the toolkit would be feature frozen; new API and new features could be added, but the existing functionality would never change—including the CSS selectors. Additionally, the new development cycle would contain a roadmap for future major API work, as well as the support window for existing stable releases, to shorten the development cycle while providing a reliable schedule.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="versioning-and-release-policy-1">Versioning and release policy</h2>
<p><img data-src="./gtk-versioning-scheme-1.png" /></p>
</section>
<section class="slide level1">

<h2 id="whats-new">What’s new</h2>
<ul>
<li class="fragment">New rendering infrastructure</li>
<li class="fragment">OpenGL by default</li>
<li class="fragment">Wayland by default</li>
<li class="fragment">Simpler backends</li>
<li class="fragment">Stable feature set</li>
<li class="fragment">Delegation, not inheritance</li>
</ul>
</section>
<section id="part-ii-documentation" class="slide level1">
<h1>Part II: Documentation</h1>
</section>
<section class="slide level1">

<h2 id="history">History</h2>
<aside class="notes">
<p>GTK started out with hand-written TexInfo pages, before migrating to a custom documentation generator that would take comments and turn them into DocBook XML first, and HTML last. This tool, called gtk-doc, served the project well enough to be reused by a whole lot of libraries that shared the same dependencies as GTK. Sadly, gtk-doc was written in Perl, and its maintenance cost followed the curve as the language’s own fall from grace. A last ditch attempt to port it to Python, as well as DocBook’s performance bottleneck that made generating the API reference take as much as compiling the whole toolkit, tests and demos included, sealed gtk-doc’s fate.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="introspection">Introspection</h2>
<aside class="notes">
<p>In the meantime, every GNOME library started providing a machine readable description of its C ABI, as well as its documentation, in the form of introspection data generated at build time. The typical consumers are language bindings, which can use it both at run time, for dynamic languages like Python or JavaScript; as well as a base for code generation, for languages like Rust or C#. Since introspection data also contains documentation, it’s possible to generate a library’s API reference much more efficiently. The GTK reference is now generated in a fraction of the time, with a better output, using a tool called gi-docgen.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="developer-documentation">Developer Documentation</h2>
<p><img data-src="./docs-gtk-org.png" /></p>
<aside class="notes">
<p>The new documentation website contains not only the GTK reference, but also the reference for the GTK dependencies; it has stable, reliable, and easy to use links; it allows stable cross-linking of symbols between libraries; and, more importantly, it is easy to fix. Every symbol has a direct link to the file that contains it, and it’s easy to update the documentation even through GitLab’s web editor; once a change is submitted, the CI pipeline will build the documentation and publish it. The new documentation website also has client-side search using fuzzy matching on a full index of symbols and terms from the documentation data.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="platform-documentation">Platform documentation</h2>
<p><img data-src="./developer-gnome-org.png" /></p>
<aside class="notes">
<p>On top of the revamped GTK documentation, the GNOME developer documentation website has been rebuilt from scratch; we moved from a custom Python 2 and Django app that would extract generated HTML from release tarballs to a Sphinx-based web site that uses plain reStructuredText. The content has been refreshed: guides and tutorials from the GNOME 2 era have been retired; examples have been updated; and there’s a new style guide for writing developer documentation in a consistent manner. The website is validated and generated via a CI pipeline, and published using GitLab pages, which should lower the barrier to contribution.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="tutorials">Tutorials</h2>
<p><img data-src="./developer-tutorials.png" /></p>
<aside class="notes">
<p>We are also in the process or writing a complete set of beginners tutorials that will bridge the gap between the Interface Guidelines and the API references, with plenty of examples. Additionally, we have advanced tutorials on how to properly integrate applications within GNOME using the various freedesktop.org specifications, as well as GNOME-specific extensions.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="gnome-interface-guidelines">GNOME interface guidelines</h2>
<p><img data-src="./developer-hig.png" /></p>
<aside class="notes">
<p>The interface guidelines have also been moved to a Sphinx-based solution, which makes them more accessible. They now reference the suggested widgets and UI components, so it’s easier to implement the patterns that make GNOME applications integrate best with the platform.</p>
</aside>
</section>
<section id="part-iii-525600-minutes" class="slide level1">
<h1>Part III: 525,600 minutes</h1>
</section>
<section class="slide level1">

<h2 id="stabilisation">Stabilisation</h2>
<aside class="notes">
<p>The main goal of GTK 4 now becomes bug fixing, and handling the feedback from the application developers porting their projects.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="gtk-as-a-portable-toolkit">GTK as a portable toolkit</h2>
<aside class="notes">
<p>Additionally, we want to ensure that GTK 4 can be used by various environments and platforms, so that it’s possible to easily port applications. We have aggressively simplified and improved our build system so it’s possible to go from cloning just the GTK repository to a full build, including the library dependencies, on both macOS and Windows, using native toolchains. Support for those platforms is part of our CI pipeline, so we can catch eventual regressions; there are still some issues, especially on macOS, but we are much more confident about the state of GTK on Windows and macOS than we have ever been.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="platform-libraries">Platform libraries</h2>
<aside class="notes">
<p>Since GTK is used by multiple environments as a foundational toolkit, we wanted to make it more platform-agnostic. Environments like Elementary and Xfce provide their own platform libraries for ensuring applications feel integrated with their guidelines. By making GTK less beholden to the GNOME design patterns, those libraries can now avoid having to undo some of the toolkit’s internal state, or write entirely custom widgets to replace GTK’s own.</p>
</aside>
</section>
<section class="slide level1">

<h2 id="libadwaita">Libadwaita</h2>
<p><img data-src="./libadwaita.png" /></p>
<aside class="notes">
<p>GNOME still needs a way to implement the UI design patterns for its own interface guidelines; additionally, GNOME’s development cycles may not match the GTK cycles—both in terms of landing new features as well as maintaining the strict API and ABI guarantees of GTK. For these reasons, the GNOME project introduced its own platform library, called “libadwaita”. Libadwaita provides UI elements that implement the GNOME style and interface guidelines, and make it easier to support things like platform-wide high contrast and dark styles. Version 1.0 of libadwaita has been released alongside GTK 4.6, and it is going to be widely available with GNOME 42.</p>
</aside>
</section>
<section id="thank-you" class="slide level1">
<h1>Thank you</h1>
</section>
<section id="questions" class="slide level1">
<h1>Questions?</h1>
</section>
<section id="resources" class="slide level1">
<h1>Resources</h1>
<ul>
<li class="fragment">GTK documentation: <a href="https://docs.gtk.org/" class="uri">https://docs.gtk.org/</a></li>
<li class="fragment">GNOME developers documentation: <a href="https://developer.gnome.org/" class="uri">https://developer.gnome.org/</a></li>
<li class="fragment">Libadwaita: <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/" class="uri">https://gnome.pages.gitlab.gnome.org/libadwaita/</a></li>
<li class="fragment">gobject-introspection: <a href="https://gi.readthedocs.org/" class="uri">https://gi.readthedocs.org/</a></li>
<li class="fragment">gi-docgen: <a href="https://gnome.pages.gitlab.gnome.org/gi-docgen/" class="uri">https://gnome.pages.gitlab.gnome.org/gi-docgen/</a></li>
</ul>
</section>
    </div>
  </div>

  <script src="reveal.js/dist/reveal.js"></script>

  <!-- reveal.js plugins -->
  <script src="reveal.js/plugin/notes/notes.js"></script>
  <script src="reveal.js/plugin/search/search.js"></script>
  <script src="reveal.js/plugin/zoom/zoom.js"></script>

  <script>

      // Full list of configuration options available at:
      // https://revealjs.com/config/
      Reveal.initialize({
      
        // Push each slide change to the browser history
        history: true,

        // reveal.js plugins
        plugins: [
          RevealNotes,
          RevealSearch,
          RevealZoom
        ]
      });
    </script>
    </body>
</html>
