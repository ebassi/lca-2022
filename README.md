These are the slides for my presentation at the 2022 linux.conf.au.

The source is the [`a-year-in-the-life.md`](./a-year-in-the-life.md)
Markdown file, which has been processed by the [Apostrophe text
editor][apostrophe] and [pandoc][pandoc] into an HTML slide deck using
the [reveal.js framework][reveal-js].

## Copyright and Licensing

Copyright 2022  Emmanuele Bassi

Released under the terms of the [CC BY-NC-SA 4.0][cc-by-nc-sa-4] license.

[apostrophe]: https://flathub.org/apps/details/org.gnome.gitlab.somas.Apostrophe
[pandoc]: https://pandoc.org/
[reveal-js]: https://revealjs.com/
[cc-by-nc-sa-4]: https://creativecommons.org/licenses/by-nc-sa/4.0/
